Profile table books_table_1 WITH select_related()
         9647 function calls (9426 primitive calls) in 0.045 seconds

Profile table books_table_1 WITHOUT select_related()
         201687 function calls (188076 primitive calls) in 1.062 seconds


*************


Profile books_table_2 WITH field arguments for select_related
         7010 function calls (6895 primitive calls) in 0.042 seconds

Profile books_table_2 WITHOUT field arguments for select_related
         11821 function calls (11500 primitive calls) in 0.056 seconds





****************************

Profile books_table_3 WITH select_related
         61870 function calls (60843 primitive calls) in 0.275 seconds


Profile books_table_3 WITH prefetch_related
         37270 function calls (34153 primitive calls) in 0.156 seconds



**********************************

Profile table books_table_1 WITH select_related()
         12011 function calls (11694 primitive calls) in 0.073 seconds

Profile table books_table_1 WITH prefetch_related()
         201972 function calls (188361 primitive calls) in 1.243 seconds


************************************

Profile table books_table_4 WITH values()
         22901 function calls (22863 primitive calls) in 0.116 seconds

Profile table books_table_4 WITHOUT values()
         34501 function calls (34481 primitive calls) in 0.184 seconds


