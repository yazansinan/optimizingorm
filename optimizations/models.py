from django.db import models


class City(models.Model):
    name = models.CharField(max_length=32)

class Person(models.Model):
    last_name = models.CharField(max_length=32)
    hometown = models.ForeignKey(City)

class Publisher(models.Model):
    name = models.CharField(max_length=32)

class Category(models.Model):
    name = models.CharField(max_length=32)
    book_count = models.IntegerField(default=0)
    
class BookManager(models.Manager):
    def books_table_1(self, num_of_books, books_qs = None):
        #Data table with Book.title, Book.author.last_name, and Book.author.city.name
        data = {}
        #allow custom queryset for orm profiling tests
        books = books_qs[:num_of_books] if books_qs is not None else self.select_related("author", "author__hometown").all()[:num_of_books]
        for book in books :
            data[book.id] = {
                'title' : book.title,
                'author_name' : book.author.last_name,
                'author_hometown' : book.author.hometown.name
            }
        
        return data
    
    def books_table_2(self, num_of_books, books_qs = None):
        #Data table with Book.title and Book.author.last_name
        data = {}
        #allow custom queryset for orm profiling tests
        books = books_qs[:num_of_books] if books_qs is not None else self.select_related("author__last_name").all()[:num_of_books]
        for book in books :
            data[book.id] = {
                'title' : book.title,
                'author_name' : book.author.last_name
            }
        
        return data
    
    def books_table_3(self, num_of_books, books_qs = None):
        #Data table with Book.title and Book.publisher.name
        data = {}
        #allow custom queryset for orm profiling tests
        books = books_qs[:num_of_books] if books_qs is not None else self.prefetch_related("publisher").all()[:num_of_books]
        for book in books :
            data[book.id] = {
                'title' : book.title,
                'publisher' : book.publisher.name
            }
        
        return data
    
    
    def books_table_4(self, num_of_books, use_values = True):
        #Data table with just Book.title
        data = {}
        #Check if values() should be used - option is there for playing with and profiling .values()
        if use_values :
            books = self.values().all()[:num_of_books]
            for book in books :
                data[book['id']] = {
                    'title' : book['title']
                }
        else :
            books = self.all()[:num_of_books]
            for book in books :
                data[book.id] = {
                    'title' : book.title
                }
            
            
        return data
    
    
class Book(models.Model):
    title = models.CharField(max_length=64)
    author = models.ForeignKey(Person)
    publisher = models.ForeignKey(Publisher)
    category = models.ForeignKey(Category)
    
    objects = BookManager()
