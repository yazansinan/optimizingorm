from django.db.models.signals import pre_save, post_delete
from django.dispatch import receiver
from optimizations.models import Book, Category

@receiver(pre_save, sender=Book)
def update_book_count_on_save(sender, instance, **kwargs):
    
    #Case where we're updating an existing Book
    if instance.id :
        instance_pre_update = Book.objects.select_related("category").get(id=instance.id)
        if instance_pre_update.category.id != instance.category.id :
            #TODO: Try to find a way to make category.book_count update on post_save to avoid
            #cases where save fails. Would still need to do update check here (in pre_save).
            instance_pre_update.category.book_count = instance_pre_update.category.book_count - 1
            instance_pre_update.category.save()
            
            instance.category.book_count = instance.category.book_count + 1
            instance.category.save()
    #Case where it's  a new 'Book'
    else : 
        category = Category.objects.get(id=instance.category.id)
        category.book_count = category.book_count + 1
        category.save()

@receiver(post_delete, sender=Book)
def update_book_count_on_delete(sender, instance, **kwargs):
    category = instance.category
    category.book_count = category.book_count - 1
    category.save()
            