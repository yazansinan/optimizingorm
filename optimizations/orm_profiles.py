import cProfile, pstats
from optimizations import utils_db
from optimizations.models import Book 



def profile_select_related():
    #Compare generating a table with select_related() and without
    setup1 = utils_db.Setup1()
    setup1.set_min_values(100,700,6,1000)
    setup1.populate()
    
    
    print("Profile table books_table_1 WITH select_related()")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_1, 
                     'x' : 100, 
                     'y' : Book.objects.select_related().all()
                     }, {})
  
   
    print("Profile table books_table_1 WITHOUT select_related()")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_1, 
                     'x' : 100, 
                     'y' : Book.objects.all()
                     }, {})
    
    
def profile_select_related_depth():    
    #Compare generating a table with select_related() arguments specified and without
    #(useful when only some of the model fields are required)
    setup1 = utils_db.Setup1()
    setup1.set_min_values(100,700,6,1000)
    setup1.populate()
    
    print("Profile books_table_2 WITH field arguments for select_related")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_2, 
                     'x' : 100, 
                     'y' : Book.objects.select_related("author__last_name").all()
                     }, {})    
        
    print("Profile books_table_2 WITHOUT field arguments for select_related")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_2, 
                     'x' : 100, 
                     'y' : Book.objects.select_related().all()
                     }, {})
    
def profile_select_prefetch_related_edge_case():
    #An edge case where prefetch_related() is better than select_related() even though
    #only ForeinKey and OneToOne fields needed. 
    #See : http://tech.yipit.com/2013/01/20/making-django-orm-queries-faster/
    setup1 = utils_db.Setup1()
    setup1.set_min_values(100,700,6,1000)
    setup1.populate()
    
    print("Profile books_table_3 WITH select_related")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_3, 
                     'x' : 1000, 
                     'y' : Book.objects.select_related("publisher__name").all()
                     }, {})    
        
    
    print("Profile books_table_3 WITH prefetch_related")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_3, 
                     'x' : 1000, 
                     'y' : Book.objects.prefetch_related("publisher").all()
                     }, {})    

def profile_values():
    #Compare generating only a dictionary of model fields resulting from query,
    #rather than model objects, in cases where only data is required (not model functionality)
    setup1 = utils_db.Setup1()
    setup1.set_min_values(100,700,6,1000)
    setup1.populate()
    
    
    print("Profile table books_table_4 WITH values()")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_4, 
                     'x' : 1000, 
                     'y' : True
                     }, {})
  
   
    print("Profile table books_table_4 WITHOUT values()")
    cProfile.runctx('f(x, y)', 
                    {'f' : Book.objects.books_table_4, 
                     'x' : 1000, 
                     'y' : False
                     }, {})
