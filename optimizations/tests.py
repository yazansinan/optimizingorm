"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

import random
from django.test import TestCase
from optimizations.models import *
from optimizations import utils_db, utils


class UtilsDbTest(TestCase):
    def test_setup_1(self):
        """
        Tests that utils_db.populate() ensure that there is the correct min# of 'Book's, 'Person's, and 'City's in db
        """
        setup1 = utils_db.Setup1()
        setup1.set_min_values(100,700,6,10,1000)
        setup1.populate()
        
        self.assertGreaterEqual(Book.objects.count(), setup1.books_min)
        self.assertGreaterEqual(Publisher.objects.count(), setup1.publishers_min)
        self.assertGreaterEqual(Category.objects.count(), setup1.categories_min)
        self.assertGreaterEqual(Person.objects.count(), setup1.persons_min)
        self.assertGreaterEqual(City.objects.count(), setup1.cities_min)

class BookManagerTest(TestCase):
    def setUp(self):
        self.setup1 = utils_db.Setup1()
        self.setup1.set_min_values(100,700,6,10,1000)
        self.setup1.populate()
                
        
class SignalsTest(TestCase):
    def setUp(self):
        self.setup1 = utils_db.Setup1()
        self.setup1.set_min_values(100,700,6,10,1000)
        self.setup1.populate()
            
    def test_update_book_count_on_save(self):
        #New book
        category = Category.objects.all()[:1].get()
        category_books_count_pre_save = category.book_count
        new_book = Book(
                           title=utils.random_word(10), 
                           author=Person.objects.all()[:1].get(),
                           publisher=Publisher.objects.all()[:1].get(),
                           category=category)
        new_book.save()
        category = Category.objects.get(id=category.id)
        self.assertEqual(category.book_count, category_books_count_pre_save + 1)
        
        #Existing book category changed
        book = Book.objects.select_related("category").all()[:1].get()
        category_original = book.category
        category_new = Category.objects.all().exclude(id=category_original.id)[:1].get()
        
        category_original_books_count_pre_save = category_original.book_count
        category_new_books_count_pre_save = category_new.book_count
        
        book.category = category_new
        book.save()
        
        category_original = Category.objects.get(id=category_original.id)
        category_new = Category.objects.get(id=category_new.id)
        
        self.assertEqual(category_original.book_count, category_original_books_count_pre_save - 1)
        self.assertEqual(category_new.book_count, category_new_books_count_pre_save + 1)
        
        #Existing book category unchanged
        book = Book.objects.select_related("category").all()[:1].get()
        category_books_count = book.category.book_count
        
        book.title = "Foundation"
        book.save()
        
        book = Book.objects.select_related("category").get(id=book.id)
        self.assertEqual(book.category.book_count, category_books_count)

    def test_update_book_count_on_delete(self):
        book = Book.objects.select_related("category").all()[:1].get()
        category = book.category
        book_count_pre_delete = category.book_count
        
        book.delete()
        category = Category.objects.get(id=category.id)
        
        self.assertEqual(category.book_count, book_count_pre_delete - 1)
        
       