import random
from optimizations.models import City, Person, Publisher, Category, Book
from optimizations import utils

class Setup1():
    cities_min = 100
    persons_min = 700
    publishers_min = 6
    categories_min = 10
    books_min = 1000
    
    def populate(self):
        
        #popualte 'City's
        self.cities_min = 100
        cities_current_count = City.objects.count()
        
        if(cities_current_count < self.cities_min): 
            cities = []    
            for i in range(self.cities_min - cities_current_count):
                cities.append(City(name=utils.random_word(10)))
            City.objects.bulk_create(cities)
            
            
        #populate 'Person's
        persons_current_count = Person.objects.count()
        
        if(persons_current_count < self.persons_min) :
            persons = []
            cities = City.objects.only("id").all()[:self.cities_min]
            for i in range(self.persons_min - persons_current_count):
                persons.append(Person(last_name=utils.random_word(7), hometown=cities[random.randint(0, len(cities) - 1)]))
            
            Person.objects.bulk_create(persons)
        
        #populate 'Publisher's
        publishers_current_count = Publisher.objects.count()
        
        if(publishers_current_count < self.publishers_min) :
            publishers = []
            for i in range(self.publishers_min - publishers_current_count) :
                publishers.append(Publisher(name=utils.random_word(15)))
            
            Publisher.objects.bulk_create(publishers)
        
        #populate 'Category's
        categories_current_count = Category.objects.count()
        
        if(categories_current_count < self.categories_min) :
            categories = []
            for i in range(self.categories_min - categories_current_count) :
                categories.append(Category(name=utils.random_word(10)))
            
            Category.objects.bulk_create(categories)
                
        #populate 'Book's
        books_current_count = Book.objects.count()
        
        if(books_current_count < self.books_min) :
            books = []
            authors = Person.objects.only("id").all()[:self.persons_min]
            publishers = Publisher.objects.only("id").all()[:self.publishers_min]
            categories = Category.objects.only("id").all()[:self.categories_min]
            for i in range(self.books_min - books_current_count) :
                books.append(
                             Book(
                                  title=utils.random_word(20), 
                                  author=authors[random.randint(0,len(authors) -1)], 
                                  publisher=publishers[random.randint(0,(len(publishers)-1))],
                                  category=categories[random.randint(0,(len(categories)-1))]))
            
            Book.objects.bulk_create(books)
            
        #update denormalized fields after bulk operations. Implementing signals for bulk_operations
        #so that this is no longer nesecary is on todo list
        categories = Category.objects.all()
        for category in categories:
            book_count = Book.objects.filter(category_id=category.id).count()
            category.book_count = book_count
            category.save()
            

    def set_min_values(self, cities_min, persons_min, publishers_min, categories_min, books_min):                
        if (cities_min > self.cities_min) :
            self.cities_min = cities_min
        if (persons_min > self.persons_min) :
            self.persons_min = persons_min
        if (publishers_min > self.publishers_min) :
            self.publishers_min = publishers_min
        if (categories_min > self.categories_min) :
            self.categories_min = categories_min
        if (books_min > self.books_min) :
            self.books_min = books_min
        
            
    
    
    
    
    
    